package com.gsmarthome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GsmartHomeApplication {

	public static void main(String[] args) {
		SpringApplication.run(GsmartHomeApplication.class, args);
	}

}
