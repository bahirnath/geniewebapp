package com.gsmarthome.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gsmarthome.model.DeviceDetails;
import com.gsmarthome.services.DeviceService;

@Controller
public class DeviceController {


	@Autowired
	private DeviceService deviceService;

	@RequestMapping("/DeviceMaster")
	public String DeviceMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		
		
		List<DeviceDetails> deviceList=deviceService.AllDeviceList();

		model.addAttribute("deviceList",deviceList);
		return "DeviceMaster";
	}

	@RequestMapping("/AddDevice")
	public String AddDevice(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		/*
		List<DeviceDetails> deviceList=deviceService.AllDeviceList();

		//List<UserDetails> userList = userDetails.getHome().getUserList();

		model.addAttribute("deviceList",deviceList);*/
		return "AddDevice";
	}
	
	  
	  @RequestMapping(value = "/AddDevice", method = RequestMethod.POST)
		 public String AddDevice(@ModelAttribute DeviceDetails devicedetails, Model model)
		 {
		  
		  System.out.println("devicedetails id ="+devicedetails.getDeviceId());

		  deviceService.saveDevice(devicedetails);
		  List<DeviceDetails> deviceList=deviceService.AllDeviceList();

			model.addAttribute("deviceList",deviceList);
			return "DeviceMaster";
		  }
	  
	
}
