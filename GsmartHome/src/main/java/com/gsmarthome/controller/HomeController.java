package com.gsmarthome.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gsmarthome.model.Home;
import com.gsmarthome.model.Room;
import com.gsmarthome.services.HomeService;
import com.gsmarthome.services.RoomService;

@Controller
public class HomeController {


	@Autowired
	private HomeService homeService;

	@RequestMapping("/HomeMaster")
	public String HomeMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		
		
		List<Home> homeList=homeService.AllHomeList();

		//List<UserDetails> userList = userDetails.getHome().getUserList();

		model.addAttribute("homeList",homeList);
		return "HomeMaster";
	}
	
}
