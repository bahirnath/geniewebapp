package com.gsmarthome.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gsmarthome.model.IOTProduct;
import com.gsmarthome.services.IOTProductService;

@Controller
public class IOTProductController {


	@Autowired
	private IOTProductService iotproductService;

	@RequestMapping("/IOTProductMaster")
	public String IOTProductMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		
		
		List<IOTProduct> iotproductList=iotproductService.AllIOTProductList();

		//List<UserDetails> userList = userDetails.getHome().getUserList();

		model.addAttribute("iotproductList",iotproductList);
		return "IOTProductMaster";
	}
}
