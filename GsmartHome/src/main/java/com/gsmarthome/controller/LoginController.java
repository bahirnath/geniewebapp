package com.gsmarthome.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;

import com.gsmarthome.model.Login;
import com.gsmarthome.repository.LoginRepository;
import com.gsmarthome.services.LoginService;
import com.gsmarthome.services.UserService;

@Controller
public class LoginController {


	@Autowired
	 LoginRepository loginRepository;

	@Autowired
	private LoginService loginService;
	
	@RequestMapping("/")
	public String Login()
	{
		
		return "Login";
	}


	@RequestMapping("/edit")
	public String Edit(@RequestParam int id,  HttpServletRequest req)
	{
		loginService.edit(id);
		return "Home";
	}
	
	   @RequestMapping(value="/login", method=RequestMethod.POST)
	    public String Login(@ModelAttribute Login login, Model model, HttpServletRequest req,HttpServletResponse res)
	    {

			String username = req.getParameter("username");
			String password = req.getParameter("password");
			
	/*
			login.setLogin_type("Admin");
			login.setStatus(1);
			login.setRole("Admin");
			*/
			Login loginDetails =loginService.view(username, password);
			
			if(loginDetails!=null)
			{

		        return "Dashboard";
			}
			else
			{

		        return "Login";
			}
			
			//loginService.saveLogin(login);
			
			
/*
			List<Login> userList=new ArrayList<Login>();
			
			userList=loginService.showAllLogin();
			
			System.out.println("userList"+userList.size());
			*/
			
	    /*	model.addAttribute("designationList", getAllDesignations());*/
	    }
	    
	 
}
