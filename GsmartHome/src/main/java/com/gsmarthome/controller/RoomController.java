package com.gsmarthome.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gsmarthome.model.Room;
import com.gsmarthome.model.UserDetails;
import com.gsmarthome.services.RoomService;

@Controller
public class RoomController {

	

	@Autowired
	private RoomService roomService;

	@RequestMapping("/RoomMaster")
	public String RoomMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		
		
		List<Room> roomList=roomService.AllRoomList();

		//List<UserDetails> userList = userDetails.getHome().getUserList();

		model.addAttribute("roomList",roomList);
		return "RoomMaster";
	}
	
}
