package com.gsmarthome.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gsmarthome.model.Switch;
import com.gsmarthome.services.SwitchService;

@Controller
public class SwitchController {


	@Autowired
	private SwitchService switchService;

	@RequestMapping("/SwitchMaster")
	public String SwitchMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		
		List<Switch> switchList=switchService.AllSwitchList();

		model.addAttribute("switchList",switchList);
		return "SwitchMaster";
	}
	
}
