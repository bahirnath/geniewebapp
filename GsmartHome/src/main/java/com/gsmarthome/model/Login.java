package com.gsmarthome.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="logindetails")
public class Login {

	@Id
	@Column(name = "id", columnDefinition = "id")
	private int id;
	@Column(name = "username", columnDefinition = "username")
	private String username;
	@Column(name = "password", columnDefinition = "password")
	private String password;
	@Column(name = "role", columnDefinition = "role")
	private String role;
	@Column(name = "login_type", columnDefinition = "login_type")
	private String login_type;
	@Column(name = "status", columnDefinition = "status")
	private int status;

	
	public Login() {}


	public Login(String username, String password, String role, String login_type, int status) {
		super();
		this.username = username;
		this.password = password;
		this.role = role;
		this.login_type = login_type;
		this.status = status;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getRole() {
		return role;
	}


	public void setRole(String role) {
		this.role = role;
	}


	public String getLogin_type() {
		return login_type;
	}


	public void setLogin_type(String login_type) {
		this.login_type = login_type;
	}


	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	@Override
	public String toString() {
		return "Login [id=" + id + ", username=" + username + ", password=" + password + ", role=" + role
				+ ", login_type=" + login_type + ", status=" + status + "]";
	}




}
