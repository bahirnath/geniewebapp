package com.gsmarthome.repository;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.model.DeviceDetails;

public interface DeviceRepository extends CrudRepository<DeviceDetails, Integer> {

}
