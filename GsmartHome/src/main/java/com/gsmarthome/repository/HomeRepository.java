package com.gsmarthome.repository;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.model.Home;

public interface HomeRepository extends CrudRepository<Home, Integer> {

}
