package com.gsmarthome.repository;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.model.IOTProduct;

public interface IOTProductRepository extends CrudRepository<IOTProduct, Integer> {

}
