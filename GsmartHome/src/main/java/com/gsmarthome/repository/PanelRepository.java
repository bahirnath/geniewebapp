package com.gsmarthome.repository;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.model.Panel;

public interface PanelRepository extends CrudRepository<Panel, Integer>  {

}
