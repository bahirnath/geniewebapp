package com.gsmarthome.repository;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.model.Room;

public interface RoomRepository extends CrudRepository<Room, Integer> {

}
