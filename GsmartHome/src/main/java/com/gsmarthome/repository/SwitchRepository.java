package com.gsmarthome.repository;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.model.Switch;

public interface SwitchRepository extends CrudRepository<Switch, Integer> {

}
