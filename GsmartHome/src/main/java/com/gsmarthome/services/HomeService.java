package com.gsmarthome.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.gsmarthome.model.Home;
import com.gsmarthome.model.UserDetails;
import com.gsmarthome.repository.HomeRepository;

@Service
@Transactional
public class HomeService {


	private final HomeRepository homeRepository;
	
	public HomeService(HomeRepository homeRepository)
	{
		this.homeRepository=homeRepository;
	}

	public List<Home> AllHomeList()
	{
		List<Home> homeList=new ArrayList<Home>();
		
		for(Home home:homeRepository.findAll())
		{
			homeList.add(home);
		}
		
		return homeList;
	}
}
