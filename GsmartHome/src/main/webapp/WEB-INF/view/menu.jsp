
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li>
                                <a href="Dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li>
                            
                            <li>
                                <a href="UserMaster"><i class="fa fa-table fa-fw"></i> User Master</a>
                            </li>
                            
                            <li>
                                <a href="HomeMaster"><i class="fa fa-table fa-fw"></i> Home Master</a>
                            </li>
                            
                            <li>
                                <a href="RoomMaster"><i class="fa fa-table fa-fw"></i> Room Master</a>
                            </li>
                            
                            <li>
                                <a href="PanelMaster"><i class="fa fa-table fa-fw"></i> Panel Master</a>
                            </li>
                            
                            <li>
                                <a href="SwitchMaster"><i class="fa fa-table fa-fw"></i> Switch Master</a>
                            </li>
                              
                            <li>
                                <a href="DeviceMaster"><i class="fa fa-table fa-fw"></i> Device Master</a>
                            </li>
                            
                            <li>
                                <a href="IOTProductMaster"><i class="fa fa-table fa-fw"></i> IOTProduct Master</a>
                            </li>
                            
                            
                            
                        </ul>
                    </div>
                </div>
